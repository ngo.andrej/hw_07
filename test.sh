FAIL=false

function test_file {
    echo "          ========================= $1 =======================          "
    if cmp --silent data/$1 out/$1 ;
    then
        echo "OK"
    else
        FAIL=true
        diff -y -W 100 data/$1 out/$1 
    fi
}

function test_code {
    echo "          ======================== return code ======================          "
    if [ $(cat "data/$1.exit") == "$2" ]
    then
        echo "OK"
    else
        FAIL=true
        echo "Not OK (returned $2, expected $(cat "data/$1.exit"))"
    fi
}

function valgrind_test {
    echo "          ========================= valgrind ========================          "
    valgrind --error-exitcode=42 --xml=yes --xml-file=/dev/null ./program < $1
    if [ "42" != "$?" ]
    then
        echo "valgrind OK"
    else
        echo "valgrind ERROR"
        valgrind ./program < $1
        FAIL=true
    fi
}

mkdir out -p
for I in data/*.in
do
    echo "==============================================================================="
    FILE=$(basename $I .in)
    ./program < $I > out/$FILE.out 2> out/$FILE.err
    test_code $FILE $?
    test_file $FILE.out # check stdout
    test_file $FILE.err # check stderr
    valgrind_test $I
done

if [ $FAIL == "false" ]
then exit 0
else exit 1
fi
